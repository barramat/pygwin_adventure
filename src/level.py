import button
import pygame

from settings import *
from sol1 import Sol1
from sol2 import Sol2
from sol3 import Sol3
from sol4 import Sol4
from sol5 import Sol5
from bloc import Bloc
from bloc_break import Bloc_break
from ice import Ice
from fish import Fish
from homard import Homard
from door import Door
from igloo import Igloo
from limit import Limit
from mask import Mask
from player import Player
from timer import Timer
from spawnl import Spawnl
from spawnr import Spawnr


class Level:
    def __init__(self, lvl, nb, c1, c2, c3, c4):

        self.nb = nb+1

        self.display_surface = pygame.display.get_surface()

        self.visible_sprites = CameraGroup()
        self.active_sprites = pygame.sprite.Group()
        self.collision_sprites = pygame.sprite.Group()
        self.fish_sprites = pygame.sprite.Group()
        self.door_sprites = pygame.sprite.Group()
        self.igloo_sprites = pygame.sprite.Group()
        self.bloc_sprites = pygame.sprite.Group()
        self.homard_sprites = pygame.sprite.Group()
        self.bloc_break_sprites = pygame.sprite.Group()
        self.spawn_sprites = pygame.sprite.Group()
        self.mask_sprites = pygame.sprite.Group()
        self.limit_sprites = pygame.sprite.Group()
        self.time = Timer()
        self.time.start()
        self.time_start = self.time.real()
        self.time_last = 0
        self.time_last_super = 0
        self.time_last_homard = 0
        self.time_at_pause = 0
        self.time_at_play = 0
        self.minute = 0
        self.seconde = 0
        self.temps = pygame.font.Font("./Font/Minecraft.ttf", 75)
        self.night = False
        pygame.mixer.init()
        pygame.mixer.music.load("sound/Pyguin_adventure_ost.mp3")
        pygame.mixer.music.play(loops=-1)
        self.bgj = pygame.image.load("./images/map/background1.png")
        self.bgn = pygame.image.load("./images/map/background2.png")
        self.P = [False,False,False,False]
        self.change = False
        self.mask_is = False
        self.homard_is = False
        self.game_paused = False
        self.lvl = lvl

        self.c1 = c1
        if self.nb > 1:
            self.c2 = c2
            if self.nb > 2:
                self.c3 = c3
                if self.nb > 3:
                    self.c4 = c4

        self.nb_fish = self.setup_level(lvl, nb)

        self.player1.set_fish(self.nb_fish)
        self.player1.set_spawn(self.spawnl,self.spawnr)
        if self.nb > 1:
            self.player2.set_fish(self.nb_fish)
            self.player2.set_spawn(self.spawnl, self.spawnr)
            if self.nb > 2:
                self.player3.set_fish(self.nb_fish)
                self.player3.set_spawn(self.spawnl, self.spawnr)
                if self.nb > 3:
                    self.player4.set_fish(self.nb_fish)
                    self.player4.set_spawn(self.spawnl, self.spawnr)

    def setup_level(self, lvl, nb):
        fish = 0
        print(nb)
        for row_index, row in enumerate(LEVEL_MAP[nb][lvl]):
            for col_index, col in enumerate(row):
                x = col_index * TILE_SIZE
                y = row_index * TILE_SIZE
                if col == '1':
                    Sol1((x, y), [self.visible_sprites, self.collision_sprites])
                if col == '2':
                    Sol2((x, y), [self.visible_sprites, self.collision_sprites])
                if col == '3':
                    Sol3((x, y), [self.visible_sprites, self.collision_sprites])
                if col == '4':
                    Sol4((x, y), [self.visible_sprites, self.collision_sprites])
                if col == '5':
                    Sol5((x, y), [self.visible_sprites, self.collision_sprites])
                if col == 'B':
                    Ice((x, y), [self.visible_sprites, self.collision_sprites])
                if col == 'P':
                    self.player1 = Player((x, y), [self.visible_sprites, self.active_sprites],
                                          [self.active_sprites, self.collision_sprites, self.fish_sprites,
                                           self.visible_sprites, self.door_sprites, self.igloo_sprites,self.bloc_sprites,
                                           self.limit_sprites, self.mask_sprites, self.homard_sprites,self.bloc_break_sprites], 1, self.c1)
                if col == 'Q':
                    self.player2 = Player((x, y), [self.visible_sprites, self.active_sprites],
                                          [self.active_sprites, self.collision_sprites, self.fish_sprites,
                                           self.visible_sprites, self.door_sprites, self.igloo_sprites, self.bloc_sprites,
                                           self.limit_sprites, self.mask_sprites, self.homard_sprites,self.bloc_break_sprites], 2, self.c2)
                if col == 'R':
                    self.player3 = Player((x, y), [self.visible_sprites, self.active_sprites],
                                          [self.active_sprites, self.collision_sprites, self.fish_sprites,
                                           self.visible_sprites, self.door_sprites, self.igloo_sprites,self.bloc_sprites,
                                           self.limit_sprites, self.mask_sprites, self.homard_sprites,self.bloc_break_sprites], 3, self.c3)
                if col == 'S':
                    self.player4 = Player((x, y), [self.visible_sprites, self.active_sprites],
                                          [self.active_sprites, self.collision_sprites, self.fish_sprites,
                                           self.visible_sprites, self.door_sprites, self.igloo_sprites, self.bloc_sprites,
                                           self.limit_sprites, self.mask_sprites, self.homard_sprites,self.bloc_break_sprites], 4, self.c4)
                if col == 'F':
                    self.fish = Fish((x, y), [self.visible_sprites, self.collision_sprites, self.fish_sprites])
                    fish += 1
                if col == 'D':
                    self.door = Door((x, y), [self.visible_sprites, self.collision_sprites, self.door_sprites])
                if col == 'I':
                    self.igloo = Igloo((x, y), [self.visible_sprites, self.collision_sprites, self.igloo_sprites])
                if col == 'Y':
                    self.limit = Limit((x, y), self.limit_sprites)
                if col == 'L':
                    self.spawnl = Spawnl((x, y), self.spawn_sprites)
                if col == 'T':
                    self.spawnr = Spawnr((x, y), self.spawn_sprites)
                if col == 'M':
                    self.mask = Mask((x, y), [self.visible_sprites, self.mask_sprites])
                    self.mask_is = True
                if col == 'X':
                    self.bloc = Bloc((x, y), [self.visible_sprites, self.bloc_sprites])
                if col == 'H':
                    self.homard = Homard((x, y), [self.visible_sprites, self.homard_sprites])
                    self.homard_is = True
                if col == 'C':
                    self.bloc_break = Bloc_break((x, y), [self.visible_sprites, self.bloc_break_sprites,
                                                              self.collision_sprites])
        if not self.mask_is:
            self.mask = Mask((-200, -300), [self.visible_sprites, self.mask_sprites])
        if not self.homard_is:
            self.homard = Homard((-200, -300), [self.visible_sprites, self.homard_sprites])
        return fish

    def run(self):
        keys = pygame.key.get_pressed()
        mx, my = pygame.mouse.get_pos()
        if keys[pygame.K_ESCAPE] and not self.game_paused:
            if self.night:
                self.display_surface.blit(self.bgn.convert_alpha(), (0, 0))
            else:
                self.display_surface.blit(self.bgj.convert_alpha(), (0, 0))
            bgpause = pygame.Rect(51, 38, 921, 691)
            pygame.draw.rect(self.display_surface, (168, 211, 228), bgpause)
            self.game_paused = True
            pygame.mixer.music.pause()
            self.time_at_pause = self.time.real() - self.time_start
        elif self.game_paused:
            if self.night:
                self.display_surface.blit(self.bgn.convert_alpha(), (0, 0))
            else:
                self.display_surface.blit(self.bgj.convert_alpha(), (0, 0))
            bgpause = pygame.Rect(51, 38, 921, 691)
            pygame.draw.rect(self.display_surface, (168, 211, 228), bgpause)
            self.minute = self.time_at_pause.real // 60
            self.seconde = (self.time_at_pause.real % 60) // 1
            temps_pause = pygame.font.Font("./font/Minecraft.ttf", 37)
            if self.seconde < 10:
                img = temps_pause.render('(' + str(int(self.minute)) + ':0' + str(int(self.seconde)) + ')', True,
                                         (0, 0, 0))
                self.display_surface.blit(img, (462, 50))
            else:
                img = temps_pause.render('(' + str(int(self.minute)) + ':' + str(int(self.seconde)) + ')', True,
                                         (0, 0, 0))
                self.display_surface.blit(img, (451, 50))
            pause_img = pygame.image.load('./images/boutons/pause.png').convert_alpha()
            pause_button = button.Button(262, 100, pause_img, 1)
            pause_button.draw(self.display_surface)
            if 712 > mx > 312 and 388 > my > 260:
                resum_img = pygame.image.load('./images/boutons/resumeHover.png').convert_alpha()
            else:
                resum_img = pygame.image.load('./images/boutons/resume.png').convert_alpha()
            resum_button = button.Button(312, 260, resum_img, 0.8)
            if resum_button.draw(self.display_surface):
                pygame.mixer.music.unpause()
                self.game_paused = False
                self.time_at_play = self.time.real() - self.time_start
                self.time_start += self.time_at_play - self.time_at_pause
            if 712 > mx > 312 and 528 > my > 400:
                leave_img = pygame.image.load('./images/boutons/restartHover.png').convert_alpha()
            else:
                leave_img = pygame.image.load('./images/boutons/restart.png').convert_alpha()
            leave_button = button.Button(312, 400, leave_img, 0.8)
            if leave_button.draw(self.display_surface):
                self.restart()
            if 712 > mx > 312 and 668 > my > 550:
                leave_img = pygame.image.load('./images/boutons/leaveHover.png').convert_alpha()
            else:
                leave_img = pygame.image.load('./images/boutons/leave.png').convert_alpha()
            leave_button = button.Button(312, 540, leave_img, 0.8)
            if leave_button.draw(self.display_surface):
                return 0
        else:
            if self.mask.grab and self.time_last_super == 0:
                self.time_last_super = self.time.real() - self.time_start
                if self.mask.nb == 1:
                    self.player1.super_hero(True)
                if self.mask.nb == 2:
                    self.player2.super_hero(True)
                if self.mask.nb == 3:
                    self.player3.super_hero(True)
                if self.mask.nb == 4:
                    self.player4.super_hero(True)
            elif 25 > self.time.real() - self.time_start - self.time_last_super >= 20:
                if self.mask.nb == 1:
                    self.player1.super_hero(False)
                if self.mask.nb == 2:
                    self.player2.super_hero(False)
                if self.mask.nb == 3:
                    self.player3.super_hero(False)
                if self.mask.nb == 4:
                    self.player4.super_hero(False)
                self.mask.nb = 0
            elif self.time.real() - self.time_start - self.time_last_super >= 25:
                self.time_last_super = 0
                self.mask.grab = False
                self.mask_sprites.add(self.mask)
                self.visible_sprites.add(self.mask)
            if self.homard.grab and self.time_last_homard == 0:
                self.time_last_homard = self.time.real() - self.time_start
                if self.homard.nb == 1:
                    self.player1.set_homard(True)
                if self.homard.nb == 2:
                    self.player2.set_homard(True)
                if self.homard.nb == 3:
                    self.player3.set_homard(True)
                if self.homard.nb == 4:
                    self.player4.set_homard(True)
            elif 25 > self.time.real() - self.time_start - self.time_last_homard >= 20:
                if self.homard.nb == 1:
                    self.player1.set_homard(False)
                if self.homard.nb == 2:
                    self.player2.set_homard(False)
                if self.homard.nb == 3:
                    self.player3.set_homard(False)
                if self.homard.nb == 4:
                    self.player4.set_homard(False)
                self.homard.nb = 0
            elif self.time.real() - self.time_start - self.time_last_homard >= 25:
                self.time_last_homard = 0
                self.homard.grab = False
                self.homard_sprites.add(self.homard)
                self.visible_sprites.add(self.homard)

            if self.night:
                self.display_surface.blit(self.bgn.convert_alpha(), (0, 0))
                if self.time.real() - self.time_start - self.time_last >= 10:
                    self.night = False
                    self.change = False
                    self.time_last = self.time.real() - self.time_start
            else:
                self.display_surface.blit(self.bgj.convert_alpha(), (0, 0))
                if self.time.real() - self.time_start - self.time_last >= 20:
                    self.night = True
                    self.change = False
                    self.time_last = self.time.real() - self.time_start

            self.player1.nuit(self.night)
            if self.nb > 1:
                self.player2.nuit(self.night)
                if self.nb > 2:
                    self.player3.nuit(self.night)
                    if self.nb > 3:
                        self.player4.nuit(self.night)

            self.player1.nuit(self.night)
            if self.nb > 1:
                self.player2.nuit(self.night)
                if self.nb > 2:
                    self.player3.nuit(self.night)
                    if self.nb > 3:
                        self.player4.nuit(self.night)

            if self.nb == 1:
                larray = [self.player1]
                self.visible_sprites.custom_draw(larray)
            if self.nb == 2:
                larray = [self.player1, self.player2]
                self.visible_sprites.custom_draw(larray)
            if self.nb == 3:
                larray = [self.player1, self.player2, self.player3]
                self.visible_sprites.custom_draw(larray)
            if self.nb == 4:
                larray = [self.player1, self.player2, self.player3, self.player4]
                self.visible_sprites.custom_draw(larray)

            self.input()
            if self.player1.update() == 5:
                self.P[0] = True
            if self.nb > 1 and self.player2.update() == 5:
                self.P[1] = True
            if self.nb > 2 and self.player3.update() == 5:
                self.P[2] = True
            if self.nb > 3 and self.player4.update() == 5:
                self.P2 = True
            valide = True
            for i in range(self.nb):
                if self.P[i] == False:
                    valide = False
            if valide:
                pygame.mixer.music.stop()
                self.P = {False, False, False, False}
                return self.time.real() - self.time_start
            bg_time = pygame.image.load('./images/boutons/backgroundTime.png').convert_alpha()
            bg_button = button.Button(0, 0, bg_time, 1)
            bg_button.draw(self.display_surface)
            bg_button = button.Button(800, 0, bg_time, 1)
            bg_button.draw(self.display_surface)
            self.minute = (self.time.real() - self.time_start) // 60
            self.seconde = ((self.time.real() - self.time_start) % 60) // 1
            if self.seconde < 10:
                img = self.temps.render(str(int(self.minute)) + ':0' + str(int(self.seconde)), True, (0, 0, 0))
            else:
                img = self.temps.render(str(int(self.minute)) + ':' + str(int(self.seconde)), True, (0, 0, 0))
            if self.minute < 10:
                self.display_surface.blit(img, (40, 20))
            else:
                self.display_surface.blit(img, (20, 20))
            fish_catch = 0
            for sprite in self.fish_sprites.sprites():
                if sprite.grab:
                    fish_catch += 1

            self.player1.set_grab(fish_catch)
            if self.nb > 1:
                self.player2.set_grab(fish_catch)
                if self.nb > 2:
                    self.player3.set_grab(fish_catch)
                    if self.nb > 3:
                        self.player4.set_grab(fish_catch)
            img = self.temps.render(str(fish_catch) + '/' + str(self.nb_fish), True, (0, 0, 0))
            self.display_surface.blit(img, (820, 20))
            bg_time = pygame.image.load('./images/Pixel arts/Autres/PoissonPng.png').convert_alpha()
            bg_button = button.Button(950, 20, bg_time, 2)
            bg_button.draw(self.display_surface)

    def restart(self):

        self.display_surface = pygame.display.get_surface()

        self.visible_sprites = CameraGroup()
        self.active_sprites = pygame.sprite.Group()
        self.collision_sprites = pygame.sprite.Group()
        self.fish_sprites = pygame.sprite.Group()
        self.door_sprites = pygame.sprite.Group()
        self.igloo_sprites = pygame.sprite.Group()
        self.bloc_sprites = pygame.sprite.Group()
        self.time = Timer()
        self.time.start()
        self.time_start = self.time.real()
        self.time_last = 0
        self.time_last_super = 0
        self.time_last_homard = 0
        self.time_at_pause = 0
        self.time_at_play = 0
        self.minute = 0
        self.seconde = 0
        self.temps = pygame.font.SysFont(None, 100)
        self.night = False
        pygame.mixer.init()
        pygame.mixer.music.load("sound/Pyguin_adventure_ost.mp3")
        pygame.mixer.music.play(loops=-1)
        self.bgj = pygame.image.load("./images/map/background1.png")
        self.bgn = pygame.image.load("./images/map/background2.png")
        self.P = [False, False, False, False]
        self.game_paused = False

        self.nb_fish = self.setup_level(self.lvl, self.nb-1)

        self.player1.set_fish(self.nb_fish)
        if self.nb > 1:
            self.player2.set_fish(self.nb_fish)
            if self.nb > 2:
                self.player3.set_fish(self.nb_fish)
                if self.nb > 3:
                    self.player4.set_fish(self.nb_fish)

    def input(self):
        keys = pygame.key.get_pressed()

        if self.nb >= 1:
            if self.night and not self.player1.homard and not self.player1.super:
                if keys[pygame.K_q] and not keys[pygame.K_d]:
                    self.player1.direction.x = 1
                elif keys[pygame.K_d] and not keys[pygame.K_q]:
                    self.player1.direction.x = -1
                else:
                    self.player1.direction.x = 0

                if keys[pygame.K_s] and self.player1.on_floor:
                    self.player1.direction.y = -self.player1.jump_speed

                if keys[pygame.K_z]:
                    self.player1.glissade = True
                else:
                    self.player1.glissade = False
                    if self.player1.time != 0:
                        self.player1.time = 0
            else:
                if keys[pygame.K_d] and not keys[pygame.K_q]:
                    self.player1.direction.x = 1
                elif keys[pygame.K_q] and not keys[pygame.K_d]:
                    self.player1.direction.x = -1
                else:
                    self.player1.direction.x = 0

                if keys[pygame.K_z] and self.player1.on_floor:
                    self.player1.direction.y = -self.player1.jump_speed

                if keys[pygame.K_s]:
                    self.player1.glissade = True
                else:
                    self.player1.glissade = False
                    if self.player1.time != 0:
                        self.player1.time = 0

        if self.nb >= 2:
            if self.night and not self.player1.homard and not self.player1.super:
                if keys[pygame.K_LEFT] and not keys[pygame.K_RIGHT]:
                    self.player2.direction.x = 1
                elif keys[pygame.K_RIGHT] and not keys[pygame.K_LEFT]:
                    self.player2.direction.x = -1
                else:
                    self.player2.direction.x = 0

                if keys[pygame.K_DOWN] and self.player2.on_floor:
                    self.player2.direction.y = -self.player2.jump_speed

                if keys[pygame.K_UP]:
                    self.player2.glissade = True
                else:
                    self.player2.glissade = False
                    if self.player2.time != 0:
                        self.player2.time = 0
            else:
                if keys[pygame.K_RIGHT] and not keys[pygame.K_LEFT]:
                    self.player2.direction.x = 1
                elif keys[pygame.K_LEFT] and not keys[pygame.K_RIGHT]:
                    self.player2.direction.x = -1
                else:
                    self.player2.direction.x = 0

                if keys[pygame.K_UP] and self.player2.on_floor:
                    self.player2.direction.y = -self.player2.jump_speed

                if keys[pygame.K_DOWN]:
                    self.player2.glissade = True
                else:
                    self.player2.glissade = False
                    if self.player2.time != 0:
                        self.player2.time = 0

        if self.nb >= 3:
            if self.night and not self.player1.homard and not self.player1.super:
                if keys[pygame.K_j] and not keys[pygame.K_l]:
                    self.player3.direction.x = 1
                elif keys[pygame.K_l] and not keys[pygame.K_j]:
                    self.player3.direction.x = -1
                else:
                    self.player3.direction.x = 0

                if keys[pygame.K_k] and self.player3.on_floor:
                    self.player3.direction.y = -self.player3.jump_speed

                if keys[pygame.K_i]:
                    self.player3.glissade = True
                else:
                    self.player3.glissade = False
                    if self.player3.time != 0:
                        self.player3.time = 0
            else:
                if keys[pygame.K_j] and not keys[pygame.K_l]:
                    self.player3.direction.x = -1
                elif keys[pygame.K_l] and not keys[pygame.K_j]:
                    self.player3.direction.x = 1
                else:
                    self.player3.direction.x = 0

                if keys[pygame.K_i] and self.player3.on_floor:
                    self.player3.direction.y = -self.player3.jump_speed

                if keys[pygame.K_k]:
                    self.player3.glissade = True
                else:
                    self.player3.glissade = False
                    if self.player3.time != 0:
                        self.player3.time = 0

        if self.nb >= 4:
            if self.night and not self.player1.homard and not self.player1.super:
                if keys[pygame.K_KP4] and not keys[pygame.K_KP6]:
                    self.player4.direction.x = 1
                elif keys[pygame.K_KP6] and not keys[pygame.K_KP4]:
                    self.player4.direction.x = -1
                else:
                    self.player4.direction.x = 0

                if keys[pygame.K_KP5] and self.player4.on_floor:
                    self.player4.direction.y = -self.player4.jump_speed

                if keys[pygame.K_KP8]:
                    self.player4.glissade = True
                else:
                    self.player4.glissade = False
                    if self.player4.time != 0:
                        self.player4.time = 0
            else:
                if keys[pygame.K_KP4] and not keys[pygame.K_KP6]:
                    self.player4.direction.x = -1
                elif keys[pygame.K_KP6] and not keys[pygame.K_KP4]:
                    self.player4.direction.x = 1
                else:
                    self.player4.direction.x = 0

                if keys[pygame.K_KP8] and self.player4.on_floor:
                    self.player4.direction.y = -self.player4.jump_speed

                if keys[pygame.K_KP5]:
                    self.player4.glissade = True
                else:
                    self.player4.glissade = False
                    if self.player4.time != 0:
                        self.player4.time = 0




class CameraGroup(pygame.sprite.Group):
    def __init__(self):
        super().__init__()
        self.display_surface = pygame.display.get_surface()
        self.offset = pygame.math.Vector2(100, 300)

        cam_left = CAMERA_BORDERS['left']
        cam_top = CAMERA_BORDERS['top']
        cam_width = self.display_surface.get_size()[0] - (cam_left + CAMERA_BORDERS['right'])
        cam_height = self.display_surface.get_size()[1] - (cam_top + CAMERA_BORDERS['bottom'])

        self.camera_rect = pygame.Rect(cam_left, cam_top, cam_width, cam_height)

    def custom_draw(self, players):
        if len(players)==1:
            self.camera_rect.left = (players[0].rect.left) - 512 + CAMERA_BORDERS['left']
        elif len(players)==2:
            if players[0].rect.left < self.camera_rect.left:
                if players[1].rect.right >= self.camera_rect.right:
                    players[0].possibleG = False
                    players[1].possibleD = False
                else:
                    players[0].possibleG = True
                    players[1].possibleD = True
            if players[0].rect.right > self.camera_rect.right:
                if players[1].rect.left <= self.camera_rect.left:
                    players[0].possibleD = False
                    players[1].possibleG = False
                else:
                    players[0].possibleD = True
                    players[1].possibleG = True

            self.camera_rect.left = (players[1].rect.left + players[0].rect.left) / 2 - 512 + CAMERA_BORDERS['left']

        elif len(players) == 3:
            left = max(players[0].rect.left, players[1].rect.left, players[2].rect.left)
            right = min(players[0].rect.left, players[1].rect.left, players[2].rect.left)
            if players[0].rect.right > self.camera_rect.right + 100:
                players[0].possibleD = False
            else:
                players[0].possibleD = True
            if players[1].rect.right > self.camera_rect.right + 100:
                players[1].possibleD = False
            else:
                players[1].possibleD = True
            if players[2].rect.right > self.camera_rect.right + 100:
                players[2].possibleD = False
            else:
                players[2].possibleD = True
            if players[0].rect.left < self.camera_rect.left:
                players[0].possibleG = False
            else:
                players[0].possibleG = True
            if players[1].rect.left < self.camera_rect.left:
                players[1].possibleG = False
            else:
                players[1].possibleG = True
            if players[2].rect.left < self.camera_rect.left:
                players[2].possibleG = False
            else:
                players[2].possibleG = True

            self.camera_rect.left = (left + right) / 2 - 512 + CAMERA_BORDERS['left']

        elif len(players) == 4:
            left = max(players[0].rect.left, players[1].rect.left, players[2].rect.left, players[3].rect.left)
            right = min(players[0].rect.left, players[1].rect.left, players[2].rect.left, players[3].rect.left)
            if players[0].rect.right > self.camera_rect.right + 100:
                players[0].possibleD = False
            else:
                players[0].possibleD = True
            if players[1].rect.right > self.camera_rect.right + 100:
                players[1].possibleD = False
            else:
                players[1].possibleD = True
            if players[2].rect.right > self.camera_rect.right + 100:
                players[2].possibleD = False
            else:
                players[2].possibleD = True
            if players[3].rect.right > self.camera_rect.right + 100:
                players[3].possibleD = False
            else:
                players[3].possibleD = True
            if players[0].rect.left < self.camera_rect.left:
                players[0].possibleG = False
            else:
                players[0].possibleG = True
            if players[1].rect.left < self.camera_rect.left:
                players[1].possibleG = False
            else:
                players[1].possibleG = True
            if players[2].rect.left < self.camera_rect.left:
                players[2].possibleG = False
            else:
                players[2].possibleG = True
            if players[3].rect.left < self.camera_rect.left:
                players[3].possibleG = False
            else:
                players[3].possibleG = True

            self.camera_rect.left = (left + right) / 2 - 512 + CAMERA_BORDERS['left']

        self.offset = pygame.math.Vector2(
            self.camera_rect.left - CAMERA_BORDERS['left'],
            self.camera_rect.top - CAMERA_BORDERS['top'])

        for sprite in self.sprites():
            offset_pos = sprite.rect.topleft - self.offset
            self.display_surface.blit(sprite.image, offset_pos)